import React, { Component } from "react";

import ContactForm from "../ContactForm/ContactForm";

class Footer extends Component {
  render() {
    return (
		<footer className="bg-dark main-footer">
			<div className="page-wrapper">
				<div className="footer-top">
					<div className="container">
						<div className="row">
							<div className="col-md-3">
								<div className="widget footer_widget">
									<h5 className="footer-title">Address</h5>
									<div className="gem-contacts">
										<div className="gem-contacts-item gem-contacts-address">
                        					<p><i className="fa fa-map-marker" aria-hidden="true"></i>
												<a target="_blank" rel="noopener noreferrer" href="https://goo.gl/maps/JggnHJi863y2Wv2bA">399 Lonsdale Street, Melbourne, VIC 3000</a>
											</p>
										</div>
                      <div className="gem-contacts-item gem-contacts-phone">
                        <i className="fa fa-phone" aria-hidden="true"></i>
                        <a href="tel:+610433926079">+61 0433 926 079</a>
                      </div>
                    </div>
                  </div>
                </div>
                    <div className="col-md-4">
                      <div className="widget footer_widget">
                        <h5 className="footer-title">Recent Posts</h5>
                        <ul className="posts  styled">
                          <li className="clearfix gem-pp-posts">
                            <div className="gem-pp-posts-text">
                              <div className="gem-pp-posts-item">
                                <a href={process.env.REACT_FRONT_PAGE_URL}>
                                  Want to start Your Own Play School.
                                </a>
                              </div>
                              <div className="gem-pp-posts-date">
                                Call +91-9122588799
                              </div>
                            </div>
                          </li>
                          <li className="clearfix gem-pp-posts">
                            <div className="gem-pp-posts-text">
                              <div className="gem-pp-posts-item">
                                <a href={process.env.REACT_FRONT_PAGE_URL}>
                                  Now we are in Faridabad,Now we are in
                                  DaudNagar.
                                </a>
                              </div>
                              <div className="gem-pp-posts-date">
                                Call +91-9122588799, +91-9122588799
                              </div>
                            </div>
                          </li>
                          <li className="clearfix gem-pp-posts">
                            <div className="gem-pp-posts-text">
                              <div className="gem-pp-posts-item">
                                <a href={process.env.REACT_FRONT_PAGE_URL}>
                                  Now we are in Ranchi, Admission open
                                </a>
                              </div>
                              <div className="gem-pp-posts-date">
                                Call +91-9122588799, +91-9122588799
                              </div>
                            </div>
                          </li>
                        </ul>
                      </div>
                    </div>
                    <div className="col-md-3">
                      <div className="widget">
                        <h5 className="footer-title">Become a Vendor</h5>
                        <div className="textwidget">
                          <ContactForm />
                        </div>
                      </div>
                    </div>

                <div className="col-md-2">
                  <div className="widget">
                    <h5 className="footer-title">Menu</h5>
                    <div className="textwidget">
                      <nav
                        id="footer-navigation"
                        className="site-navigation footer-navigation centered-box"
                        role="navigation"
                      >
                        <ul
                          id="footer-menu"
                          className="nav-menu styled clearfix"
                        >
                          <li className="menu-item">
                            <p>
                              <a href={process.env.REACT_FRONT_PAGE_URL}>
                                Support
                              </a>
                            </p>
                          </li>
                          <li className="menu-item">
                            <p>
                              <a href={process.env.REACT_FRONT_PAGE_URL}>
                                Contact Us
                              </a>
                            </p>
                          </li>
                          <li className="menu-item">
                            <p>
                              <a href={process.env.REACT_FRONT_PAGE_URL}>
                                Disclaimer
                              </a>
                            </p>
                          </li>
                          <li className="menu-item">
                            <p>
                              <a href={process.env.REACT_FRONT_PAGE_URL}>
                                Privacy
                              </a>
                            </p>
                          </li>
                        </ul>
                      </nav>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="footer-bottom">
            <div className="container">
              <div className="row">
                <div className="col-md-6">
                  <div className="footer-site-info">
                    2020 © ATMC Final Year Project
                  </div>
                </div>

                <div className="col-md-6">
                  <div id="footer-socials">
                    <div className="socials inline-inside socials-colored">
                      <a
                        href={process.env.REACT_FRONT_PAGE_URL}
                        target="_blank"
                        rel="noopener noreferrer"
                        title="Facebook"
                        className="socials-item"
                      >
                        <i className="fa fa-facebook-f facebook"></i>
                      </a>
                      <a
                        href={process.env.REACT_FRONT_PAGE_URL}
                        target="_blank"
                        rel="noopener noreferrer"
                        title="Twitter"
                        className="socials-item"
                      >
                        <i className="fa fa-twitter twitter"></i>
                      </a>
                      <a
                        href={process.env.REACT_FRONT_PAGE_URL}
                        target="_blank"
                        rel="noopener noreferrer"
                        title="Instagram"
                        className="socials-item"
                      >
                        <i className="fa fa-instagram instagram"></i>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </footer>
    );
  }
}

export default Footer;
