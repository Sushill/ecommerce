import React, { Component } from "react";
import axios from "axios";

class ContactForm extends Component {
  state = {
    name: "",
    message: "",
    email: "",
    sent: false,
    buttonText: "Send Message",
  };

  formSubmit = (e) => {
    e.preventDefault();

    this.setState({
      buttonText: "...sending",
    });

    let data = {
      name: this.state.name,
      email: this.state.email,
      message: this.state.message,
    };

    axios
      .post(`${process.env.REACT_APP_API_URL}/contactform/send`, data)
      .then((res) => {
        this.setState({ sent: true }, this.resetForm());
      })
      .catch(() => {
        console.log("Message not sent");
      });
  };

  resetForm = () => {
    this.setState({
      name: "",
      message: "",
      email: "",
      buttonText: "Message Sent",
    });
  };

  render() {
    return (
      <div className="contact">
        <form
          id="contact-form"
          onSubmit={(e) => this.formSubmit(e)}
          method="POST"
        >
          <div className="form-group">
            <input
              type="text"
              className="form-control"
              id="name"
              name="name"
              placeholder="Full Name"
              onChange={(e) => this.setState({ name: e.target.value })}
              value={this.state.name}
            />
          </div>
          <div className="form-group">
            <input
              type="email"
              className="form-control"
              id="email"
              aria-describedby="emailHelp"
              placeholder="Email Address"
              name="email"
              onChange={(e) => this.setState({ email: e.target.value })}
              value={this.state.email}
            />
          </div>
          <div className="form-group">
            <textarea
              className="form-control"
              rows="5"
              id="formMessage"
              placeholder="Message"
              name="message"
              onChange={(e) => this.setState({ message: e.target.value })}
              value={this.state.message}
            ></textarea>
          </div>
          <button type="submit" className="btn btn-primary">
            {this.state.buttonText}
          </button>
        </form>
      </div>
    );
  }
}

export default ContactForm;
