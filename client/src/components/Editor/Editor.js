import React, { Component } from "react";

import DataTable from "react-data-table-component";

import Icon1 from "@material-ui/icons/ReplyAll";
import Icon2 from "@material-ui/icons/Markunread";
import Icon3 from "@material-ui/icons/CloudDownload";
import TextField from "@material-ui/core/TextField";

import data from "./data";

const columns = [
  {
    name: "Thumbnail",
    selector: "thumbnail",
    sortable: false,
  },
  {
    name: "Title",
    selector: "title",
    sortable: true,
  },
  {
    name: "Categories",
    selector: "categories",
    sortable: true,
  },
  {
    name: "Status",
    selector: "status",
    sortable: true,
  },
  {
    name: "Date",
    selector: "date",
    sortable: true,
  },
  {
    name: "Action",
    selector: "action",
    sortable: false,
  },
];

export default class Editor extends Component {
  render() {
    return (
      <div>
        <DataTable
          title="Movie List"
          columns={columns}
          data={data}
          defaultSortField="title"
          selectableRows
          selectableRowsHighlight
          selectableRowsVisibleOnly
          pagination
          striped
          subHeader
          subHeaderComponent={
            <div style={{ display: "flex", alignItems: "center" }}>
              <TextField
                id="outlined-basic"
                label="Search"
                variant="outlined"
                size="small"
                style={{ margin: "5px" }}
              />
              <Icon1 style={{ margin: "5px" }} color="action" />
              <Icon2 style={{ margin: "5px" }} color="action" />
              <Icon3 style={{ margin: "5px" }} color="action" />
            </div>
          }
          subHeaderAlign="right"
        />
      </div>
    );
  }
}

// import React, { Component } from "react";
// import FroalaEditor from "react-froala-wysiwyg";

// class Editor extends Component {
//   constructor(props) {
//     super(props);

//     this.state = {
//       model: "model",
//     };

//     this.onChangeHandler = this.onChangeHandler.bind(this);
//   }

//   onChangeHandler = (model) => {
//     this.setState({
//       model: model,
//     });
//   };
//   render() {
//     return (
//       <div className="Editor">
//         <div className="Editor">
//           <div className="editor">
//             <FroalaEditor
//               model={this.state.model}
//               onModelChange={this.onChangeHandler}
//             />
//           </div>
//         </div>
//       </div>
//     );
//   }
// }

// export default Editor;
