import React, { useState } from "react";
import { Link, Redirect } from "react-router-dom";
import { signin, authenticate } from "../../helpers/authFetch";
import Facebook from "../FacebookLogin/FaceBookLogin";

const Signin = () => {
  const [values, setValues] = useState({
    email: "",
    password: "",
    error: "",
    loading: false,
    redirectToReferrer: false,
  });

  const { email, password, loading, error, redirectToReferrer } = values;

  const handleChange = (name) => (event) => {
    setValues({ ...values, error: false, [name]: event.target.value });
  };

  const clickSubmit = (event) => {
    event.preventDefault();
    setValues({ ...values, error: false, loading: true });
    signin({ email, password }).then((data) => {
      if (data.error) {
        setValues({ ...values, error: data.error, loading: false });
      } else {
        authenticate(data, () => {
          setValues({
            ...values,
            redirectToReferrer: true,
          });
        });
      }
    });
  };

const signUpForm = () => (
	<div class="row justify-content-center">
		<div class="col-md-4">
			<div class="">
				<div className="text-center">
					{/* <Google informParent={informParent} /> */}
					<Facebook informParent={informParent} />
				</div>
			</div>
			<div class="regContainer">
				<h1 className="title auth-heading">Sign In</h1>
				<form>
					<div className="form-group">
						<label className="text-muted">Email</label>
						<input onChange={handleChange("email")} type="email" className="form-control" value={email}/>
					</div>
					<div className="form-group">
						<label className="text-muted">Password</label>
						<input onChange={handleChange("password")} type="password" className="form-control" value={password} />
					</div>
					<div class="form-group">
						<p class="mb-0 fs-14">Don't remember your password? <Link to="/auth/password/forgot">&nbsp;Click here!</Link></p>
					</div>
					<div class="form-group mb-0 text-right">
						<button onClick={clickSubmit} className="btn btn-primary">Submit</button>
					</div>
				</form>
			</div>
		</div>
	</div>
);

  const showError = () => (
    <div className="alert alert-danger" style={{ display: error ? "" : "none" }}>
      {error}
    </div>
  );

  const showLoading = () =>
    loading && (
      <div className="d-flex justify-content-center">
        <div className="spinner-border" role="status">
          <span className="sr-only">Loading...</span>
        </div>
      </div>
    );

  const redirectUser = () => {
    if (redirectToReferrer) {
      return <Redirect to="/" />;
    }
  };

  const informParent = (response) => {
    authenticate(response, () => {
      setValues({
        ...values,
        redirectToReferrer: true,
      });
    });
  };

return (
	<section class="auth_bg sec-padd-100">
		<div className="container">
			{showLoading()}
			{showError()}
			{signUpForm()}
			{redirectUser()}
		</div>
	</section>
  );
};

export default Signin;
