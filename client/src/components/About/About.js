import React from 'react';

const Home = () => {
  return (
	<div class="about-us">
		<div class="container-fluid">
    <div class="row">
    <div class="col-lg-3 col-md-4 col-sm-6 mb-4" id="test">
      <div class="card h-100">
          <img src="https://previews.123rf.com/images/azvector/azvector1803/azvector180300411/97269455-login-icon-authorize-icon-log-in-sign-login-icon-open-account-symbol-register-new-user-vector-icon.jpg" alt="" class="img-fluid rounded-circle w-50" id="wow"></img> 
          <div class="card-body"> 
                
          <h4 class="card-title">
          <a href="#"> Login/Signup system</a>
          </h4>
          <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae.</p>
        </div>
      </div>
    </div>
    <div class="col-lg-3 col-md-4 col-sm-6 mb-4">
      <div class="card h-100">
        <img src="https://previews.123rf.com/images/coolvectorstock/coolvectorstock1808/coolvectorstock180803567/106920241-dashboard-icon-vector-isolated-on-white-background-for-your-web-and-mobile-app-design-dashboard-logo.jpg" alt="" class="img-fluid rounded-circle w-50" id="wowOne"></img>
        <div class="card-body">
          <h4 class="card-title">
            <a href="#">User/Admin dashboard</a>
          </h4>
          <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae.</p>
        </div>
      </div>
    </div>
    <div class="col-lg-3 col-md-4 col-sm-6 mb-4">
      <div class="card h-100">
        <img src="https://previews.123rf.com/images/alekseyvanin/alekseyvanin1805/alekseyvanin180500293/100678046-rotation-arrows-with-lock-vector-icon-filled-flat-sign-for-mobile-concept-and-web-design-update-pass.jpg" alt="" class="img-fluid" id="wowTwo"></img>
        <div class="card-body">
          <h4 class="card-title">
            <a href="#">Reset password via email</a>
          </h4>
          <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae.</p>
        </div>
      </div>
    </div>
    <div class="col-lg-3 col-md-4 col-sm-6 mb-4">
      <div class="card h-100">
      <img src="https://secure.webtoolhub.com/static/resources/icons/set112/30367b0.png" alt="" class="img-fluid rounded-circle w-50" id="wowThree"></img>
        <div class="card-body">
          <h4 class="card-title">
            <a href="#">CRUD products, categories</a>
          </h4>
          <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae.</p>
        </div>
      </div>
    </div>
    <div class="col-lg-3 col-md-4 col-sm-6 mb-4">
      <div class="card h-100">
        <img src="https://previews.123rf.com/images/luka007/luka0071505/luka007150500204/39574212-shopping-cart-icon.jpg" alt="" class="img-fluid rounded-circle w-50" id="wowThree"></img>
        <div class="card-body">
          <h4 class="card-title">
            <a href="#">Shopping cart</a>
          </h4>
          <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae.</p>
        </div>
      </div>
    </div>
    <div class="col-lg-3 col-md-4 col-sm-6 mb-4">
      <div class="card h-100">
      <img src="https://previews.123rf.com/images/brokenhill/brokenhill1909/brokenhill190900034/132548270-data-filter-icon-on-white-background-vector-illustration.jpg" alt="" class="img-fluid rounded-circle w-50" id="wowThree"></img>        
      <div class="card-body">
          <h4 class="card-title">
            <a href="#">Products filtered by category and price</a>
          </h4>
          <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae.</p>
        </div>
      </div>
    </div>
    <div class="col-lg-3 col-md-4 col-sm-6 mb-4">
      <div class="card h-100">
      <img src="https://previews.123rf.com/images/alekseyvanin/alekseyvanin1809/alekseyvanin180900332/107525809-mobile-message-notification-vector-icon-filled-flat-sign-for-mobile-concept-and-web-design-smartphon.jpg"  class="img-fluid rounded-circle w-50" id="wowThree"></img>
        <div class="card-body">
          <h4 class="card-title">
            <a href="#">Order notification</a>
          </h4>
          <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae.</p>
        </div>
      </div>
    </div>
    <div class="col-lg-3 col-md-4 col-sm-6 mb-4">
      <div class="card h-100">
      <img src="https://previews.123rf.com/images/eljanstock/eljanstock1811/eljanstock181117954/111613932-coin-vector-icon-isolated-on-transparent-background-coin-transparency-logo-concept.jpg" alt="" class="img-fluid rounded-circle w-50" id="wowThree"></img>
        <div class="card-body">
          <h4 class="card-title">
            <a href="#">Payment with PayPal</a>
          </h4>
          <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae.</p>
        </div>
      </div>
    </div>
    </div>
	</div>
  </div>
  );
};

export default Home;
