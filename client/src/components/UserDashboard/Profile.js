import React, { useState, useEffect } from 'react';
import { isAuthenticated } from '../../helpers/authFetch';
import {
  getUser,
  updateUser,
  updateUserLocalStorage
} from '../../helpers/user';

const Profile = ({ match }) => {
  const [values, setValues] = useState({
    name: '',
    email: '',
    password: '',
    error: '',
    success: false
  });

  const { name, email, password, error, success } = values;
  const { token } = isAuthenticated();
  useEffect(() => {
    getUser(match.params.userId, token).then(data => {
      if (data.error) {
        setValues({ ...values, error: true });
      } else {
        setValues({ ...values, name: data.name, email: data.email });
      }
    });
    // eslint-disable-next-line
  }, []);

  const handleChange = name => event => {
    // console.log(event.target.value);
    setValues({ ...values, error: false, [name]: event.target.value });
  };

  const clickSubmit = event => {
    event.preventDefault();
    updateUser(match.params.userId, token, { name, email, password }).then(
      data => {
        if (data.error) {
          alert(data.error);
        } else {
          updateUserLocalStorage(data, () => {
            setValues({
              ...values,
              name: data.name,
              email: data.email,
              success: true
            });
          });
        }
      }
    );
  };

  const showSuccess = success => (
    <div
      className="alert alert-info"
      style={{ display: success ? '' : 'none' }}
    >
      Your profile has been updated successfully!
    </div>
  );

  const showError = error => (
    <div
      className="alert alert-danger"
      style={{ display: error ? '' : 'none' }}
    >
      Fail to update your profile
    </div>
  );

const updateForm = () => (
	<form>
		<div class="row justify-content-center">
			<div class="col-md-8 pl-0">
				<div class="col-md-12">
					<div class="headingstyle1">
						<h2>Profile update</h2>
					</div>
				</div>
				<div class="col-md-12">
					<div className="form-group">
						<label className="text-muted">Name</label>
						<input onChange={handleChange('name')} value={name} type="text" className="form-control" />
					</div>
				</div>
				<div class="col-md-12">
					<div className="form-group">
						<label className="text-muted">Email</label>
						<input defaultValue={email} type="email" className="form-control" disabled />
					</div>
				</div>
				<div class="col-md-12">
					<div className="form-group">
						<label className="text-muted">Password</label>
						<input onChange={handleChange('password')} value={password} type="password" className="form-control" />
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group text-right">
						<button className="btn btn-primary" onClick={clickSubmit}>Update</button>
					</div>
				</div>
			</div>
		</div>
    </form>
  );

return (
	<div>
	<section class="banner-inner">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="banner-heading">
							<h2>Your Profile</h2>
						</div>
					</div>
				</div>
			</div>
		</section>
	<section class="sec-padd">
		<div className="container">
			<div class="row">
			</div>
			{showSuccess(success)}
			{showError(error)}
			{updateForm()}
    	</div>
	</section>
	</div>
  );
};

export default Profile;
