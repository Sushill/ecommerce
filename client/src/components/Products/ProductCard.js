import React from "react";
import { Link } from "react-router-dom";
import { Button } from "shards-react";
import axios from "axios";

const ProductCard = (props) => {
  const product = props.product;

  const values = [];

  if (props.product.uncategorized) {
    values.push("Uncategorized");
  }
  if (props.product.design) {
    values.push("Design");
  }
  if (props.product.development) {
    values.push("Development");
  }
  if (props.product.writing) {
    values.push("Writing");
  }

  return (
    <tr role="row" className="odd">
      <td className="file-manager__item-icon" tabIndex="0">
        <div>
          <img src={product.profileImg} className="img-responsive" alt="test" />
        </div>
      </td>
      <td className="text-left">
        <h5 className="file-manager__item-title">
          <Link className="text-fiord-blue" to={`/edit-product/${product._id}`}>
            {product.title}
          </Link>
        </h5>
        <span className="file-manager__item-meta">
          Last opened 3 months ago.
        </span>
      </td>
      <td className="sorting_1">{values.join(", ")}</td>
      <td>{product.status}</td>
      <td className="date column-date" data-colname="Date">
        Published
        <br />
        <abbr title={product.date}>{product.date}</abbr>
      </td>
      <td className="file-manager__item-actions">
        <div
          className="btn-group btn-group-sm d-flex justify-content-end"
          role="group"
          aria-label="Table row actions"
        >
          <Link className="text-fiord-blue" to={`/edit-product/${product._id}`}>
            <button type="button" className="btn btn-white active-light">
              <i className="material-icons"></i>
            </button>
          </Link>

          <Button type="button" size="sm" theme="danger">
            <i className="material-icons">delete</i>
          </Button>
        </div>
      </td>
    </tr>
  );
};

export default ProductCard;
