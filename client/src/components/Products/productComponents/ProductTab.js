import React, { Component } from "react";

import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import "react-tabs/style/react-tabs.css";

import Description from "./description";
import Facts from "./facts";
import DatePrice from "./datePrice";
import Gallery from "./gallery";
import FAQ from "./faq";
import Download from "./download";
export default class ProductTab extends Component {
  render() {
    return (
      <Tabs>
        <TabList>
          <Tab>Description</Tab>
          <Tab>Facts</Tab>
          <Tab>Prices/stock</Tab>
          <Tab>Gallery</Tab>
          <Tab>FAQs</Tab>
          <Tab>Download</Tab>
        </TabList>

        <TabPanel>
          <div>
            <Description></Description>
          </div>
        </TabPanel>

        <TabPanel>
          <Facts></Facts>
        </TabPanel>
        <TabPanel>
          <DatePrice></DatePrice>
        </TabPanel>
        <TabPanel>
          <Gallery></Gallery>
        </TabPanel>
        <TabPanel>
          <FAQ></FAQ>
        </TabPanel>
        <TabPanel>
          <Download></Download>
        </TabPanel>
      </Tabs>
    );
  }
}
