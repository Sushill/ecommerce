import React, { Component } from "react";
import { Row, Col, Card, CardBody, Form, FormInput } from "shards-react";
import axios from "axios";

import "./product.css";

import Editor from "../editor/editor";
import SidebarActions from "../common/SidebarActions";
import SidebarCategories from "../common/SidebarCategories";
import FeaturedImage from "../common/FeaturedImage";

export default class FinalProduct extends Component {
  constructor(props) {
    super(props);

    this.state = {
      title: "",
      tripDescription: "",
      profileImg: "",
      uncategorized: false,
      design: false,
      development: false,
      writing: false,
    };

    // this.fileInput = React.createRef();
    this.onChangeHandler = this.onChangeHandler.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
  }

  handleInputChange = (e) => {
    const target = e.target;
    const name = target.name;
    const value = target.value;
    this.setState({
      [name]: value,
    });
  };
  onChangeHandler = (e) => {
    this.setState({
      profileImg: e.target.files[0],
    });
    console.log(e.target.files[0]);
  };

  handleSubmit = (e) => {
    e.preventDefault();
    const data = new FormData();
    data.append("title", this.state.title);
    data.append("description", this.state.tripDescription);
    axios
      .post(`${process.env.REACT_APP_API_URL}/products`, data, {})
      .then((res) => {
        this.setState({
          title: "",
          tripDescription: "",
          profileImg: "",
          status: "Draft",
          visibility: "Public",
          schedule: "Now",
          readability: "ok",
          uncategorized: false,
          design: false,
          development: false,
          writing: false,
        });
        //this.props.history.push("/blog-products");
      })
      .catch((err) => {
        console.log("Error in Createpage!");
      });
  };

  render() {
    return (
      <Row>
        <Form id="form1" className="add-new-page" onSubmit={this.handleSubmit}>
          <Col lg="12" md="12">
            <Card small className="mb-3">
              <CardBody>
                <FormInput
                  type="text"
                  placeholder="Title of the Product"
                  name="title"
                  className="form-control mb-3"
                  value={this.state.title}
                  onChange={this.handleInputChange}
                  size="lg"
                />

                <Editor />

                <div className="custom-file mb-3">
                  <input
                    onChange={this.onChangeHandler}
                    type="file"
                    accept="image/*"
                    name="Choose Image"
                    className="custom-file-input"
                    id="customFile2"
                  />
                  <label className="custom-file-label" htmlFor="customFile2">
                    choose image ....
                  </label>
                </div>

                <br />
                <br />
              </CardBody>
            </Card>
          </Col>

          {/* Sidebar Widgets */}
          <Col lg="3" md="12">
            <SidebarActions></SidebarActions>
            <SidebarCategories></SidebarCategories>
            <FeaturedImage></FeaturedImage>
          </Col>
        </Form>
      </Row>
    );
  }
}
