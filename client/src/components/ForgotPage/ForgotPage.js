import React, { useState } from 'react';
import { forgotPasswordFetch } from '../../helpers/authFetch';

const ForgotPage = () => {
  const [values, setValues] = useState({
    email: '',
    error: '',
    success: false
  });

  const { email, error, success } = values;

  const handleChange = event => {
    setValues({ ...values, [event.target.name]: event.target.value });
  };

  const clickSubmit = event => {
    event.preventDefault();
    setValues({ ...values, error: false });
    forgotPasswordFetch({ email }).then(data => {
      if (data.error) {
        setValues({ ...values, error: data.error });
      } else {
        setValues({ ...values, error: false, success: true });
      }
    });
  };

  const forgotPasswordForm = () => (
	<div class="row justify-content-center">
		<div class="col-md-4">
			<div class="regContainer">
				<h1 className="title pt-3 pb-3 auth-heading">Forgot Password</h1>
				<form>
      				<div className="form-group">
						<input onChange={handleChange} name="email" value={email} type="email" placeholder="Email" className="form-control" />
					</div>
					<div class="form-group text-right">
						<button className="btn btn-primary" onClick={clickSubmit}>Submit</button>
					</div>
    			</form>
			</div>
		</div>
	</div>
  );

  const showError = () => (
    <div
      className="alert alert-danger"
      style={{ display: error ? '' : 'none' }}
    >
      {error}
    </div>
  );

  const showSuccess = () => (
    <div
      className="alert alert-info"
      style={{ display: success ? '' : 'none' }}
    >
      We sent a reset password link to your email!
    </div>
  );

return (
<section class="auth_bg sec-padd-100">
	<div className="container">
		{showError()}
		{showSuccess()}
		{forgotPasswordForm()}
	</div>
</section>
  );
};

export default ForgotPage;
