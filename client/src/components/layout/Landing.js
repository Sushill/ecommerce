import React, { Component } from "react";
import { Link } from "react-router-dom";

import Navbar from "./Navbar";
class Landing extends Component {
  render() {
    return (
      <div className="wrapper">
        <Navbar />
        <div
          style={{ height: "75vh" }}
          className="container valign-wrapper text-center"
        >
          <div className="row">
            <div className="col s12 center-align">
              <h4>Login/Register for a Vendor or Login as an Admin</h4>
              <p className="flow-text grey-text text-darken-1">
                If you don't have an account please Register.
              </p>
              <br />
              <div className="col s6">
                <Link
                  to="/register"
                  style={{
                    width: "140px",
                    borderRadius: "3px",
                    letterSpacing: "1.5px",
                  }}
                  className="btn btn-large waves-effect waves-light hoverable blue accent-3"
                >
                  Register
                </Link>
              </div>
              <div className="col s6">
                <Link
                  to="/login"
                  style={{
                    borderRadius: "3px",
                    letterSpacing: "1.5px",
                  }}
                  className="btn btn-large btn-flat waves-effect yellow  black-text"
                >
                  Admin Log In
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default Landing;
