import React, { Component } from "react";
import { Link } from "react-router-dom";
class Navbar extends Component {
  render() {
    return (
      <div className="navbar-fixed text-center">
        <nav className="z-depth-0">
          <div className="nav-wrapper white">
            <Link
              to="/admin"
              style={{
                fontFamily: "monospace",
              }}
              className="col s4 brand-logo center black-text"
            >
              <i className="material-icons">code</i>
              Dashboard Login
            </Link>
          </div>
        </nav>
      </div>
    );
  }
}
export default Navbar;
