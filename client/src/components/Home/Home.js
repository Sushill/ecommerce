import React, { useState, useEffect } from 'react';
import { getProducts } from '../../helpers/userFetch';
import Card from './Card';
import jquery from 'jquery';
import Shop from '../Shop/Shop';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import "slick-carousel/slick/slick.js";
import Slider from "react-slick";

const Home = () => {

  const [productsBySold, setProductBySold] = useState([]);
  const [productsByArrival, setProductByArrival] = useState([]);
  const [error, setError] = useState(false);
  const [loading, setLoading] = useState(true);

  const loadProductsBySold = () => {
    getProducts('sold').then(data => {
      if (data.error) {
        setError(data.error);
      } else {
        setLoading(false);
        setProductBySold(data);
      }
    });
  };

  const loadProductsByArrival = () => {
    getProducts('createdAt').then(data => {
      if (data.error) {
        setError(data.error);
      } else {
        setLoading(false);
        setProductByArrival(data);
      }
    });
  };
  useEffect(() => {
    loadProductsByArrival();
    loadProductsBySold();
    // eslint-disable-next-line
  }, []);

  const showError = () => error && <h2>Fail to load!</h2>;

  const showLoading = () =>
    loading && (
      <div className="d-flex justify-content-center">
        <div className="spinner-border" role="status">
          <span className="sr-only">Loading...</span>
        </div>
      </div>
    );
	
	const settings1 = {
		dots: false,
		arrows: false,
        infinite: true,
        speed: 800,
        slidesToShow: 1,
        slidesToScroll: 1
	}
	const settings2 = {
		dots: true,
		arrows: false,
        speed: 800,
        slidesToShow: 4,
        slidesToScroll: 1,
    }

  return (

<div>

<section>
    <Slider {...settings1}>
        <div class="slider-assets">
            <img src={process.env.PUBLIC_URL + '/1.jpg'} class="img-fluid" id="sliderCont" />
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="view-product">
                            <a href="javascript:;">View Product</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="slider-assets">
            <img src={process.env.PUBLIC_URL + '/2.jpg'} class="img-fluid" id="sliderCont2" />
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="view-product">
                            <a href="javascript:;">View Product</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="slider-assets">
            <img src={process.env.PUBLIC_URL + '/3.jpg'} class="img-fluid" id="sliderCont3" />
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="view-product">
                            <a href="javascript:;">View Product</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </Slider>
</section>

<section class="best-seller sec-padd">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="headingstyle1">
					<h2>Best Seller</h2>
					<p>Here are the best seller products</p>
				</div>
			</div>
		</div>
	</div>
    <Slider {...settings2}>
        <div class="slider-assets mr-auto">
            <img src={process.env.PUBLIC_URL + '/p1.png'} class="img-fluid" id="sliderCont" />
        </div>
        <div class="slider-assets mr-auto">
            <img src={process.env.PUBLIC_URL + '/p1.png'} class="img-fluid" id="sliderCont2" />
        </div>
        <div class="slider-assets mr-auto">
            <img src={process.env.PUBLIC_URL + '/p1.png'} class="img-fluid" id="sliderCont3" />
        </div>
		<div class="slider-assets mr-auto">
            <img src={process.env.PUBLIC_URL + '/p1.png'} class="img-fluid" id="sliderCont3" />
        </div>
		<div class="slider-assets mr-auto">
            <img src={process.env.PUBLIC_URL + '/p1.png'} class="img-fluid" id="sliderCont3" />
        </div>
		<div class="slider-assets mr-auto">
            <img src={process.env.PUBLIC_URL + '/p1.png'} class="img-fluid" id="sliderCont3" />
        </div>
    </Slider>
</section>

<section class="home-shop sec-padd">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="headingstyle1">
					<h2>Our Products</h2>
					<p>Having Spent Years In The Industry, We Have A Wide Range of Products</p>
				</div>
			</div>
		</div>
	</div>
	<Shop></Shop>
</section>

</div>
  );
};

export default Home;