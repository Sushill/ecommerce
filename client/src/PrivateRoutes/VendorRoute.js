import React from "react";
import { Route, Redirect } from "react-router-dom";
import { isAuthenticated } from "../helpers/authFetch";

import Navbar from "../components/Navbar/Navbar";
import Footer from "../components/Footer/Footer";

const VendorRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={(props) =>
      isAuthenticated() && isAuthenticated().user.role === 1 ? (
        <div>
          <Navbar />
          <Component {...props} />

          <Footer />
        </div>
      ) : (
        <Redirect
          to={{
            pathname: "/",
            state: { from: props.location },
          }}
        />
      )
    }
  />
);

export default VendorRoute;
