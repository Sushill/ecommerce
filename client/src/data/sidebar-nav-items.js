export default function () {
  return [
    {
      id: 1,
      title: "Blog Dashboard",
      to: "/admin",
      htmlBefore: '<i class="material-icons">edit</i>',
      htmlAfter: "",
      content: [],
    },
    {
      id: 2,
      title: "Blog",
      htmlBefore: '<i class="material-icons">vertical_split</i>',
      to: "#",
      htmlAfter: '<i class="fas fa-chevron-down"></i>',
      content: [
        {
          id: 2.1,
          title: "Blog Posts",
          htmlBefore: '<i class="material-icons">vertical_split</i>',
          to: "/admin/blog-posts",
        },
        {
          id: 2.2,
          title: "Add New Post",
          htmlBefore: '<i class="material-icons">note_add</i>',
          to: "/admin/add-new-post",
        },
      ],
    },
    {
      id: 3,
      title: "Page",
      htmlBefore: '<i class="material-icons">vertical_split</i>',
      to: "#",
      htmlAfter: '<i class="fas fa-chevron-down"></i>',
      content: [
        {
          id: 3.1,
          title: "All Page",
          htmlBefore: '<i class="material-icons">vertical_split</i>',
          to: "/admin/pages",
        },
        {
          id: 3.2,
          title: "Add New Page",
          htmlBefore: '<i class="material-icons">note_add</i>',
          to: "/admin/add-new-page",
        },
      ],
    },
    {
      id: 4,
      title: "Products",
      htmlBefore: '<i class="material-icons">vertical_split</i>',
      to: "#",
      htmlAfter: '<i class="fas fa-chevron-down"></i>',
      content: [
        {
          id: 4.1,
          title: "All Products",
          htmlBefore: '<i class="material-icons">vertical_split</i>',
          to: "/admin/products",
        },
        {
          id: 4.2,
          title: "Add New Product",
          htmlBefore: '<i class="material-icons">note_add</i>',
          to: "/admin/add-new-product",
        },
      ],
    },
    {
      id: 5,
      title: "Features",
      htmlBefore: '<i class="material-icons">vertical_split</i>',
      to: "/admin/feature",
      htmlAfter: '<i class="fas fa-chevron-down"></i>',
      content: [
        {
          id: 5.1,
          title: "Menu",
          to: "#",
        },
        {
          id: 5.2,
          title: "Users",
          to: "#",
        },
      ],
    },
    // {
    //   id: 7,
    //   title: "Forms & Components",
    //   htmlBefore: '<i class="material-icons">view_module</i>',
    //   to: "/components-overview",
    //   content: []
    // },
    // {
    //   id: 8,
    //   title: "Tables",
    //   htmlBefore: '<i class="material-icons">table_chart</i>',
    //   to: "/tables",
    //   content: []
    // },
    // {
    //   id: 9,
    //   title: "User Profile",
    //   htmlBefore: '<i class="material-icons">person</i>',
    //   to: "/user-profile-lite",
    //   content: []
    // },
    // {
    //   id: 10,
    //   title: "Errors",
    //   htmlBefore: '<i class="material-icons">error</i>',
    //   to: "/errors",
    //   content: []
    // }
  ];
}
