import React, { Component } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import jwt_decode from "jwt-decode";
import setAuthToken from "./utils/setAuthToken";

//route of admin section
import routes from "./routes";

//Components
import Home from "./components/Home/Home";
import Register from "./components/Register/Register";
import SignIn from "./components/SignIn/SignIn";
import Navbar from "./components/Navbar/Navbar";
import Footer from "./components/Footer/Footer";
import UserDashboard from "./components/UserDashboard/UserDashboard";
import About from "./components/About/About";
import Shop from "./components/Shop/Shop";
import Search from "./components/Search/Search";
import Product from "./components/Product/Product";
import Cart from "./components/Cart/Cart";

//PrivateRoutes folder
import PrivateRoute from "./PrivateRoutes/PrivateRoute";
import Profile from "./components/UserDashboard/Profile";

import { setCurrentUser, logoutUser } from "./actions/authActions";
import { Provider } from "react-redux";
import store from "./store";

//SuperAdminDashboard
import SuperAdminRoute from "./PrivateRoutes/SuperAdminRoute";
import Editor from "./components/Editor/Editor";

//VendorDashboard folder
import VendorRoute from "./PrivateRoutes/VendorRoute";
import VendorDashboard from "./components/VendorDashboard/VendorDashboard";
import CreateCategory from "./components/VendorDashboard/CreateCategory";
import CreateProduct from "./components/VendorDashboard/CreateProduct";
import Orders from "./components/VendorDashboard/Order";
import ManageProducts from "./components/VendorDashboard/ManageProducts";
import ManageCategories from "./components/VendorDashboard/ManageCategories";
import UpdateCategory from "./components/VendorDashboard/UpdateCategory";
import UpdateProduct from "./components/VendorDashboard/UpdateProduct";
import ForgotPage from "./components/ForgotPage/ForgotPage";
import ResetPage from "./components/ResetPage/ResetPage";

const NavRoute = ({ exact, path, component: Component }) => (
  <Route
    exact={exact}
    path={path}
    render={(props) => (
      <div>
        <Navbar />
        <Component {...props} />
        <Footer />
      </div>
    )}
  />
);

const App = () => {

  return (
    <>
      <Router>
        <Switch>
          {/* User Routes */}
          <NavRoute exact path="/" component={Home} />
          <NavRoute exact path="/register" component={Register} />
          <NavRoute exact path="/signin" component={SignIn} />
          <NavRoute exact path="/shop" component={Shop} />
          <NavRoute exact path="/about" component={About} />
          <NavRoute exact path="/search" component={Search} />
          <NavRoute exact path="/product/:productId" component={Product} />
          <NavRoute exact path="/cart" component={Cart} />
          <NavRoute exact path="/auth/password/forgot" component={ForgotPage} />
          <NavRoute
            exact
            path="/auth/password/reset/:token"
            component={ResetPage}
          />
          <PrivateRoute
            exact
            path="/user/dashboard"
            component={UserDashboard}
          />
          <PrivateRoute exact path="/profile/:userId" component={Profile} />
          {/* Admin Routes */}
          <VendorRoute
            exact
            path="/vendor/dashboard"
            component={VendorDashboard}
          />
          <VendorRoute
            exact
            path="/create/category"
            component={CreateCategory}
          />
          <VendorRoute exact path="/create/product" component={CreateProduct} />
          <VendorRoute exact path="/vendor/orders" component={Orders} />
          <VendorRoute
            exact
            path="/vendor/products"
            component={ManageProducts}
          />
          <VendorRoute
            exact
            path="/vendor/product/update/:productId"
            component={UpdateProduct}
          />
          <VendorRoute
            exact
            path="/vendor/categories"
            component={ManageCategories}
          />
          <VendorRoute
            exact
            path="/vendor/category/update/:categoryId"
            component={UpdateCategory}
          />
        </Switch>
      </Router>
    </>
  );
};

export default App;
