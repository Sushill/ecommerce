import React from "react";

import Navbar from "../components/Navbar/Navbar";
import Footer from "../components/Footer/Footer";

const FrontPage = ({ children, noNavbar, noFooter }) => (
  <div className="container-fluid">
    <div className="row">
      <Navbar />
      {children}
      <Footer />
    </div>
  </div>
);

export default FrontPage;
