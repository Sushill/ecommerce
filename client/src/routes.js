// // Layout Types
import { DefaultLayout } from "./layouts";

//SuperAdminDashboard
import SuperAdminDashboard from "./components/SuperAdminDashboard/SuperAdminDashboard";

import ExtraFeature from "./views/ExtraFeatures";

import UserProfileLite from "./views/UserProfileLite";
import AddNewPost from "./views/AddNewPost";
import Errors from "./views/Errors";
// import ComponentsOverview from "./views/ComponentsOverview";
// import Tables from "./views/Tables";
import BlogPosts from "./views/BlogPosts";
import UpdatePostInfo from "./components/Post/UpdatePostInfo";
import AddNewPage from "./views/AddNewPage";
import Pages from "./views/Pages";
import UpdatePageInfo from "./components/Page/UpdatePageInfo";

// import AddNewProduct from "./views/AddNewProduct";
// import Products from "./views/Products";
// import UpdateProductInfo from "./components/Products/UpdateProductInfo";

export default [
  { path: "/dashboard", layout: DefaultLayout, component: SuperAdminDashboard },
  {
    path: "/admin/feature",
    layout: DefaultLayout,
    component: ExtraFeature,
  },
  {
    path: "/admin/edit-post/:id",
    layout: DefaultLayout,
    component: UpdatePostInfo,
  },
  {
    path: "/admin/edit-page/:id",
    layout: DefaultLayout,
    component: UpdatePageInfo,
  },
  //   {
  //     path: "/admin/edit-product/:id",
  //     layout: DefaultLayout,
  //     component: UpdateProductInfo,
  //   },
  {
    path: "/admin/pages",
    layout: DefaultLayout,
    component: Pages,
  },
  //   {
  //     path: "/admin/products",
  //     layout: DefaultLayout,
  //     component: Products,
  //   },
  {
    path: "/admin/user-profile-lite",
    layout: DefaultLayout,
    component: UserProfileLite,
  },
  {
    path: "/admin/add-new-post",
    layout: DefaultLayout,
    component: AddNewPost,
  },
  {
    path: "/admin/add-new-page",
    layout: DefaultLayout,
    component: AddNewPage,
  },
  //   {
  //     path: "/admin/add-new-product",
  //     layout: DefaultLayout,
  //     component: AddNewProduct,
  //   },
  {
    path: "/admin/errors",
    layout: DefaultLayout,
    component: Errors,
  },
  {
    path: "/admin/blog-posts",
    layout: DefaultLayout,
    component: BlogPosts,
  },
];
