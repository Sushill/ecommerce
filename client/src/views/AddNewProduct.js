import React from "react";
import { Container, Row } from "shards-react";

import PageTitle from "../components/common/PageTitle";

import FinalTour from "../components/Tour/FinalTour";

const AddNewProduct = () => (
  <Container fluid className="main-content-container px-4 pb-4">
    {/* Page Header */}
    <Row noGutters className="page-header py-4">
      <PageTitle
        sm="4"
        title="Add New Product"
        subtitle="Products"
        className="text-sm-left"
      />
    </Row>
    <FinalTour />
  </Container>
);

export default AddNewProduct;
